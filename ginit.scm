#!/usr/bin/env sh
exec guix shell guile guile-gnutls -- guile --no-auto-compile -L . -e main -s "$0" "$@"
!#

(use-modules (ice-9 ports))
(use-modules (web client))
(use-modules (ice-9 receive))

(define (main args)
  (begin
    (let* ([project-name (list-ref args 1)]
	   [package-definition (string-append project-name "/" "guix.scm")]
	   [.envrc (string-append project-name "/" ".envrc")]
	   [copyfile (string-append project-name "/" "COPYING")]
	   [.dir-locals (string-append project-name "/" ".dir-locals.el")])
      (mkdir project-name)
      (call-with-output-file package-definition
	(lambda (port)
	  (format port
		  "(use-modules
               (jeko-packages)
	       (gnu packages guile)
	       (gnu packages guile-xyz)
	       (guix packages)
	       (guix build-system guile)
	       ((guix licenses) #:prefix license:))

	      (package
	       (name \"\")
	       (version \"\")
	       (build-system guile-build-system)
	       (source \"\")
	       (inputs (list guile-3.0 guile-gunit64))
	       (propagated-inputs (list))
	       (home-page \"\")
	       (synopsis \"\")
	       (description \"\" )
	       (license license:gpl3+))
")))
      (call-with-output-file .envrc
	(lambda (port)
	  (format port
		  "eval $(guix shell -D -f guix.scm --search-paths)
export GUILE_LOAD_PATH=$PWD:$GUILE_LOAD_PATH
export GUILE_LOAD_COMPILED_PATH=$PWD:$GUILE_LOAD_COMPILED_PATH
")))
      (call-with-output-file .dir-locals
	(lambda (port)
	  (format port
		  "((scheme-mode . ((eval . (setq geiser-debug-treat-ansi-colors 'colors))
				    (eval . (setq geiser-debug-jump-to-debug nil)))))
")))
      (copy-file (string-append (dirname (current-filename)) "/COPYING")
		 (string-append project-name "/COPYING"))
      (mkdir (string-append project-name "/" "tests"))
      (unless (member "--without-git" args)
       (system* "git" "init" "--quiet" project-name)
       (let ([git-dir (string-append "GIT_DIR=" project-name)]
	     [guix-file (canonicalize-path package-definition)])
	(system* "git" "-C" (canonicalize-path project-name) "add" ".")
	(system* "git" "-C" (canonicalize-path project-name) "commit" "--quiet" "-am" "'Initial commit'"))))))
