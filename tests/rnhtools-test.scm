(use-modules (srfi srfi-64))

(use-modules ((f) #:prefix f:)
             ((f ports) #:prefix p:))

(define TMP "/tmp/rnhtools-files/")

(define (test-asset filename)
  (string-append TMP filename))

(define (cleanup)
  (delete-file (test-asset "guix.scm"))
  (delete-file (test-asset ".envrc"))
  (delete-file (test-asset "COPYING"))
  (f:delete (test-asset ".git") #t)
  (rmdir (test-asset "tests"))
  (rmdir TMP))

(test-begin "red-nose-hacker-tools")

(test-group-with-cleanup
    "ginit"

  (system* "./ginit.scm" TMP)

  (test-assert "should create a guix.scm file"
    (access? (test-asset "guix.scm") F_OK))

  (test-assert "should create a tests dir"
    (access? (test-asset "tests/") F_OK))

  (test-assert "should create a .envrc file"
    (access? (test-asset "COPYING") F_OK))

  (test-assert "should create a COPYING file"
    (access? (test-asset "COPYING") F_OK))

  (cleanup))

(test-end "red-nose-hacker-tools")

